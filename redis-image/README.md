# Creating a Dockerfile workflow

- Specify a base image
- Run install commands to install additional programs
- Specify a command to run on container startup

## Dockerfile Syntaxes

Every line has:
- An instruction
- Argument to instruction

    Example:
    - `FROM alpine` (`FROM` instruction specifies base image that we want to use - that is `alpine` as the base image)
        - `alpine`, base image, is an analogy to an `OS` that comes with a preinstalled set of programs that are useful for developers. `Docker` image is like a `VM` with no `OS` installed, thus `alpine` Docker image is an analogy to initial `OS` which has a set of programs inside of it that is needed to be installed.

    - `RUN apk add --update redis` (`RUN` instruction executes somme commands in preparing our custom image. In this case run `apk add --update redis`)
        - `apk`, package manager, comes pre-installed in `alpine` base image.

    - `CMD ["redis-server"]`(`CMD` instruction specifies what is, `redis-server`, executed when our image is used to startup a brand new container)

Docker Build Process In Detail

Dockerfile feeds it to Docker Client feeds it to Docker Server which builds an Usable Image.

Dockerfile -> Docker Client -> Docker Server -> Usable Image

`docker build .` is what we gives Dockerfile to Docker CLI that builds an Image. 

Tagging an image - `docker run <hash-id>` is annoying instead build with a tag `docker build docker-id/project-name:version` to use the image tag in `docker run image-tag`. For example, `docker build -t saiwaimaung/redis:latest .` then run with `docker run saiwaimaung/redis`  