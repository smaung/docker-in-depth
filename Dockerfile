### BEGINNING OF BUILD_PHASE BLOCK
# base image tag as build phase
FROM node:alpine as build_phase
# set WORKDIR
WORKDIR /app
# copy over package.json to install dependencies. # . will be /app
COPY package.json .
# install dependencies
RUN npm install
# copy over source code
COPY . .
RUN npm run build
### END OF BUILD_PHASE BLOCK

### RUN_PHASE STARTS
FROM nginx as run_phase
# all we need is /app/build from BUILD_PHASE. dest path from https://hub.docker.com/_/nginx
COPY --from=build_phase /app/build /usr/share/nginx/html
# run nginx which is the default CMD command of the base image, so no need to have another CMD command