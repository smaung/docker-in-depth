const express = require('express');

const app = express()

app.get('/', (req, res) => {
    res.send('Greeting from Sai :)');
});

app.listen(8946, () => {
    console.log('Listening on port 8946');
});