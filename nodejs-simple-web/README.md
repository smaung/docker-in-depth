# Steps to write a Dockerfile for a Nodejs web application container

1) <span style='color:red'>**[REQUIRED]**</span> Specify a base `image:version`. For example, a node application would need nodejs installation. In this case, we choose `node:alpine` as our base image available via `https://hub.docker.com/_/node` with `alpine` version since it's the most stripped down light weight Linux image. Or we can choose `alpine` as the base image and install node via `RUN apk add --update node` as the other option.

2) <span style='color:yellow'>*[Optional]*</span> but best practices by setting workspace in the image FileSystem(FS) snapshot to avoid file names conflicting in subsequent `COPY` instructions. 
     - `WORKDIR /home/apps`
     - with this all subsequent instructions will be executed in the above directory. 

3) <span style='color:red'>**[REQUIRED]**</span> Copy required application source codes to the image FS snapshot by executing:
  -  `COPY ./index.js ./` 
  -  `COPY ./package.json ./` 
  
4) <span style='color:red'>[REQUIRED]</span> Install dependencies of web application.
   - install dependencies: `npm install`

5) <sparn style='color:red'>[REQUIRED]</span> Container startup command:
   - start nodejs server: `CMD ["npm", "start"]`

# Docker Runtime Port Mapping
- Port Forwarding - from localhost to container, is strictly runtime constraint. Nothing need to be added in Dockerfile. 
  
  `docker run -p 8946:8946 <image tag/id>`
  - first port is localhost port forwarding to the second container port. Source and destination ports don't have to be identical. 
  - after this command, access to web browser `localhost:8946` will have backward response from nodejs server. 
  
# Attaching Running Docker Container Shell
We can achieve this in two ways:
- `docker run -it <REPOSITORY:TAG/CONTAINER ID> sh`
- `docker exec -it <CONTAINER ID> sh`
  - `docker image ls` to get all images downloaded to local for info on `<REPOSITORY:TAG>`
  - `docker ps` to see all running containers to get info like `<CONTAINER ID>`
- Both instructions will take the user to `WORKDIR` set in `Dockerfile`
  
# Writing Dockerfile best practices

Instructions order matter, the following snipper is better:

    `COPY ./package.json ./`
    `RUN npm install`
    `COPY ./index.js ./`

Than

    `COPY ./index.js ./`
    `COPY ./package.json ./`
    `RUN npm install`

Because `npm install` only required `package.json`, and when we make changes in `index.js` it would only run from that instruction and any instructions prior would be pulled from cache. Otherwise the second snippet would has three instruction changes in total whereas the first snippet only has one instruction change.
