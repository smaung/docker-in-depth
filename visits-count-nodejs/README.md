# Docker Compose

A separate CLI that comes together with `Docker`. 
- Used to start up multiple containers at the same time
- Used for networking between multiple containers

## Docker Compose Files

- `docker-compose.yml` contains all the options we'd normally pass to `docker-cli`
- `docker-compose.yml` goes into `docker-compose CLI`
- `docker-compose` shares the same network when creating multiple containers using `docker-compose.yml`. For example, in log message 'Creating network "project_name_default" with the default driver'

## Docker Commands

- `docker-compose up` to run all services(containers) in `docker-compose.yml`
- `docker-compose up --build` is equivalent to two commands `docker build` and `docker run`
- `docker-compose build` to rebuild images
- `docker run -d image-tag` to run a container in the background. `docker ps` to get the container id, then `docker stop <container id>` to stop running container. This workflow is trouble when working with multiple containers. `docker-compose` enable running and stopping multiple containers at the same time with one single command. `docker compose up -d` `docker-compose down`

## Handling Crashed Containers (Maintenance)

Four Restart Policies:

1) **"no"** - never attempt to restart crashed container. Noted the quote cos of conflict with yml no which is a boolean false. Policy is in string "no"
2) **always** - always attempt to restart crashed container for ANY reason including exit code 0 (SUCCESS)
3) **on-failure** - restart on error that is non-zero exit code
4) **unless-stopped** - always restart unless developer forcibly stop it

## Container Status with docker-compose
`docker-compose ps` to see Ports mapping. Run it from the same dir where docker-compose.yml resides.
Example, `visits-count-nodejs_node-js_1        docker-entrypoint.sh npm start   Up      0.0.0.0:8081->8964/tcp`

`visits-count-nodejs_redis-server_1   docker-entrypoint.sh redis ...   Up      6379/tcp`