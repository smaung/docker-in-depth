const express = require('express');
const redis = require('redis');
const process = require('process');

const app = express();
const redisCli = redis.createClient({
    'host': 'redis-server' // service name in docker-compose.yml
});
const numVisits = 'num_visits';
redisCli.set(numVisits, 0);

app.get('/', (req, res) => {
    redisCli.get(numVisits, (err, visits_num) => {
        res.send('Total number of visits to this page: ' + visits_num);
        redisCli.set(numVisits, parseInt(visits_num) + 1);
    });
});

app.listen(8964, () => {
    console.log('LISTEN ON PORT: 8964');
});