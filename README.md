### Status
[![Build Status](https://travis-ci.org/saimaung/docker-in-depth.png)](https://travis-ci.org/saimaung/docker-in-depth)
[![Coverage Status](https://codecov.io/gh/saimaung/docker-in-depth/badge.png?branch=master)](https://codecov.io/gh/saimaung/docker-in-depth?branch=master)


This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify

# Dockerfile Workflow
## Dev
Create a Dockerfile.dev for development. In this we will run `npm run start`. To build a custom `Dockerfile.dev`, run `docker build -f Dockerfile.dev .`

### Docker Volume
- once a development server inside a container started, changes to source files need to copy over to the image snapshot again, then rebuild. This is annoying. Use `docker volume` that maps `localvolume:containervolume`.
  - `docker run -p 3000:3000 -v /app/node_modules -v $(pwd):/app <image-id>`
    - first -v is a place holder, that is don't map node_modules to local drive.
    - to avoid running this long command, use `docker-compose.yml` instead.
    - use `docker exec -it <container id> npm run test` to attach to currently running container by `docker-compose` to avoid creating another identical container just for the test, and to avoid all the volume mounting etc setting
    - `docker attach <container-id>` attaching to primary process `stdin, stdout, stderr` of the running container